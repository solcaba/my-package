# CHANGELOG



## v0.1.1 (2024-03-26)

### Fix

* fix(deploy): add deploy pipeline ([`5ac5bde`](https://gitlab.com/solcaba/my-package/-/commit/5ac5bdeea26735e09679a2a14a29eceb16d6ff67))


## v0.1.0 (2024-03-26)

### Feature

* feat(sr): add semantic release cd pipeline ([`b3f1666`](https://gitlab.com/solcaba/my-package/-/commit/b3f1666c62263fa22d960363b18a4825df17e9bb))

### Unknown

* add semantic_release config ([`9315527`](https://gitlab.com/solcaba/my-package/-/commit/931552791516a5e996fe9c90daaad3fde231193e))

* add python-semantic-release dev dependecy ([`160faac`](https://gitlab.com/solcaba/my-package/-/commit/160faac50cc1b3652dac2d24b4604cf30cbea762))

* Add pyproject.toml to init poetry ([`e129e80`](https://gitlab.com/solcaba/my-package/-/commit/e129e8075dd1de3c77d4ddbbd18c790b7c36436b))

* Update .gitlab-ci.yml file ([`14c5276`](https://gitlab.com/solcaba/my-package/-/commit/14c5276d4a1acbb756ded73429081496ab2c18d9))

* Add fist pipeline ([`7b38da2`](https://gitlab.com/solcaba/my-package/-/commit/7b38da2d85a21cf475d82f6c1dffb2a517ac9999))

* Initial commit ([`723e07a`](https://gitlab.com/solcaba/my-package/-/commit/723e07a7b8fdbac6c45c8b864802b2ae1387b193))
